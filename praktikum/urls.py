from django.conf.urls import include
from django.urls import re_path
from django.contrib import admin
from django.views.generic.base import RedirectView

urlpatterns = [
    re_path(r'^admin/', admin.site.urls),
    re_path(r'^Story-1/', include('Story_1.urls')),
    re_path(r'^Story-4/', include('Story_4.urls')),
    re_path(r'^Story-5/', include('Story_5.urls')),
    re_path(r'^$', RedirectView.as_view(url='/Story-5/', permanent=True)),
]
