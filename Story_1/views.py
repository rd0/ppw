from django.shortcuts import render
from datetime import datetime, date

mhs_name = 'Yusuf Tri Ardho'
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(1998, 6, 7)
npm = 1706074985
pp = 'https://scontent-sit4-1.cdninstagram.com/vp/6570c9f70602376d7a88ed91edc0440a/5C25CF72/t51.2885-19/s150x150/15877384_1297942060229328_6361335311179448320_a.jpg'

def index(request):
    response = {'name': mhs_name, 
    			'age': calculate_age(birth_date.year), 
    			'npm': npm,
    			'profpic': pp}
    return render(request, 'index_Story1.html', response)

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0
