from django.shortcuts import render, redirect
from .forms import ScheduleForm
from .models import Event

def mySchedule(request):
	schedules = Event.objects.all().values().order_by('-date', '-time')
	response = {
		'schedules': schedules
	}
	return render(request, 'mySchedule.html', response)

def addSchedule(request):
	if request.method == 'POST':
		form = ScheduleForm(request.POST)
		if form.is_valid():
			schedule = Event()
			schedule.date = form.cleaned_data['date']
			schedule.time = form.cleaned_data['time']
			schedule.name = form.cleaned_data['name']
			schedule.venue = form.cleaned_data['venue']
			schedule.category = form.cleaned_data['category']
			schedule.save()
			return redirect('/Story-5')
		else:
			return mySchedule(request)
	else:
		form = ScheduleForm()
		response = {
			'form': form
		}
		return render(request, 'addSchedule.html', response)

def deleteAll(request):
	schedules = Event.objects.all()
	for event in schedules:
		event.delete()
	return redirect('/Story-5')
