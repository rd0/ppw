from django import forms
from .models import CATEGORIES

class ScheduleForm(forms.Form):
    date = forms.DateTimeField(widget=forms.DateInput(attrs={
        'class': 'input',
        'required': True,
        'type': 'date',
    }))

    time = forms.TimeField(widget=forms.TimeInput(attrs={
        'class': 'input',
        'required': True,
        'type': 'time',
    }))

    name = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'input',
        'placeholder': 'Schedule Name',
        'required': True,
    }))
    
    venue = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'input',
        'placeholder': 'Venue',
        'required': True,
    }))

    category = forms.ChoiceField(choices=CATEGORIES, widget=forms.Select(attrs={
        'class': 'input',
        'placeholder': 'Category',
        'required': True,
    }))