from django.db import models
from datetime import datetime, timedelta

# Create your models here.

CATEGORIES =[
	("Class", "Class"),
	("Lab", "Lab"),
	("Task", "Task"),
	("Quiz", "Quiz"),
]

class Event(models.Model):
    date = models.DateTimeField()
    time = models.CharField(max_length=10)
    name = models.CharField(default='', max_length=50)
    venue = models.CharField(default='', max_length=50)
    category = models.CharField(choices=CATEGORIES, max_length=10)
