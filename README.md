# Tutorials and Assignment Repository

CSGE602022 - Web Design & Programming (Perancangan & Pemrograman Web) @
Faculty of Computer Science Universitas Indonesia, Odd Semester 2018/2019

* * *

## Table of Contents

Welcome to the code repository.
This repository hosts weekly tutorial codes and other, such as course-related
code snippets.

1. Weekly Exercises
    1. [Story 1](Story_1/README.md) - Intro to WebPro
    2. [Story 2](Story_2/README.md) - Design Your Own Website
    3. [Story 3](Story_3/README.md) - HTML Bootstrap
    4. [Story 4](Story_4/README.md) - Django (Views + Template)
    5. [Story 5](Story_5/README.md) - Models